package account

import (
	"time"
)

const (
	PTypeMD5    = "MD5"
	PTypeSHA1   = "SHA1"
	PTypeSHA256 = "SHA256"
)

const (
	UTypeCustomer = "customer"
	UTypeAdmin    = "admin"
)

const (
	StatusOK     = 0
	StatusBanned = 1
)

type Account struct {
	TableName struct{}  `sql:"account" json:"-"`
	ID        string    `sql:"id" json:"id"`
	Email     string    `sql:"email" json:"email"`
	Salt      string    `sql:"salt" json:"-"`
	Password  string    `sql:"passwd" json:"-"`
	PType     string    `sql:"ptype" json:"-"`
	UType     string    `sql:"utype" json:"utype"`
	Status    int       `sql:"status" json:"status"`
	UTime     time.Time `sql:"utime,null" json:"utime"`
	CTime     time.Time `sql:"ctime,null" json:"ctime"`
}

func (a *Account) Auth(plain string) bool {
	password, _ := encryptPassword(a.Salt, plain, a.PType)
	return password == a.Password
}

type Token struct {
	TableName struct{}  `sql:"token" json:"-"`
	ID        string    `sql:"id" json:"id"`
	UserID    string    `sql:"user_id" json:"user_id"`
	ClientID  string    `sql:"client_id" json:"client_id"`
	ETime     time.Time `sql:"etime" json:"etime"`
	CTime     time.Time `sql:"ctime" json:"ctime"`
}

type UserProfile struct {
	TableName struct{}  `sql:"user_profile" json:"-"`
	UserID    string    `sql:"user_id,pk" json:"user_id"`
	Name      string    `sql:"name" json:"name"`
	Avatar    string    `sql:"avatar" json:"avatar"`
	Cover     string    `sql:"cover" json:"cover"`
	UTime     time.Time `sql:"utime,null" json:"utime"`
	CTime     time.Time `sql:"ctime,null" json:"ctime"`
}
