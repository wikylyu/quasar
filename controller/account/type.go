package account

type LoginRequest struct {
	Email    string `json:"email" form:"email" query:"email" validate:"required"`
	Password string `json:"password" form:"password" query:"password" validate:"required"`
}

type SignupRequest struct {
	LoginRequest
}
