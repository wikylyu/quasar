.PHONY: all quasar star clean

BIN=./bin

all: quasar star

quasar:
	CGO_ENABLED=0 go build -o $(BIN)/$@ main.go

star:
	CGO_ENABLED=0 go build -o $(BIN)/$@ worker/star/*.go

clean:
	rm -rf bin/*

docker:
	docker build --target star .
	docker build --target quasar .

install:
	mkdir -p /opt/quasar/star
	cp $(BIN)/quasar /opt/quasar
	cp $(BIN)/star /opt/quasar/star
	cp -n script/supervisord/quasar.conf /etc/supervisor/conf.d
	cp -n quasar.yaml /opt/quasar
	ln -s -f /opt/quasar/quasar.yaml /opt/quasar/star/quasar.yaml
