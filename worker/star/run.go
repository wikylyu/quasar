package main

import (
	"time"

	log "github.com/sirupsen/logrus"
	ssModel "gitlab.gnome.org/wikylyu/quasar/model/ss"
	"gitlab.gnome.org/wikylyu/quasar/ssm"
)

func updateFlows(now time.Time, ssinfo *ssModel.UserSSInfo, nodes []*ssModel.Node, service *ssModel.UserService) {
	for _, node := range nodes {
		for i := 0; i < 2; i++ {
			t := now.Add(time.Duration(0-i) * time.Hour)
			year := t.Year()
			month := int(t.Month())
			day := t.Day()
			hour := t.Hour()
			var ingress, egress, gIngress, gEgress, nIngress, nEgress uint64
			stat, err := ssModel.GetUserNodeHourlyFlowStat(ssinfo.UserID, node.ID, year, month, day, hour)
			if err != nil {
				log.Error("GetUserMonthlyFlowStat failed:", ssinfo.UserID)
				continue
			} else if stat != nil {
				ingress = stat.Ingress
				egress = stat.Egress
			}
			gstat, _ := ssm.StatTunnel(ssinfo, node, year, month, day, hour)
			if gstat != nil {
				gIngress = gstat.Ingress
				gEgress = gstat.Egress
			}

			if ingress <= gIngress {
				nIngress = gIngress - ingress
			}
			if egress <= gEgress {
				nEgress = gEgress - egress
			}
			if nIngress > 0 || nEgress > 0 {
				ssModel.AddUserNodeHourlyFlowStat(ssinfo.UserID, node.ID, year, month, day, hour, nIngress, nEgress)
				ssModel.AddUserNodeDailyFlowStat(ssinfo.UserID, node.ID, year, month, day, nIngress, nEgress)
				ssModel.AddUserNodeMonthlyFlowStat(ssinfo.UserID, node.ID, year, month, nIngress, nEgress)

				ssModel.AddUserHourlyFlowStat(ssinfo.UserID, year, month, day, hour, nIngress, nEgress)
				ssModel.AddUserDailyFlowStat(ssinfo.UserID, year, month, day, nIngress, nEgress)
				ssModel.AddUserMonthlyFlowStat(ssinfo.UserID, year, month, nIngress, nEgress)

				service.FlowUsed += nIngress + nEgress
			}
		}
	}
}

/* 更新流量 */
func runFlows() {
	nodes, err := ssModel.FindNodes()
	if err != nil {
		log.Error("FindNodes failed")
		return
	}
	now := time.Now()

	/* 获取当前服务的流量 */
	services, err := ssModel.FindUserServiceWithStatus(ssModel.ServiceStatusRunning)
	if err != nil {
		log.Error("FindUserServiceWithStatus failed")
		return
	}
	for _, service := range services {
		ssinfo, err := ssModel.GetUserSSInfo(service.UserID)
		if err != nil {
			log.Error("GetUserSSInfo failed:", service.UserID)
			continue
		}
		updateFlows(now, ssinfo, nodes, service)

		/* 更新service状态 */
		service.UpdateStatus(now)
		ssModel.UpdateUserService(service.ID, service.Status, service.FlowUsed)
		if service.Status != ssModel.ServiceStatusRunning {
			ssm.StopTunnels(ssinfo, nodes)
		}
	}

	/* 将为开始的服务设置为开始 */
	services, err = ssModel.FindUserServiceWithTime(now, ssModel.ServiceStatusWaiting)
	if err != nil {
		log.Error("FindUserServiceWithTime failed")
	}
	for _, service := range services {
		ssinfo, err := ssModel.GetUserSSInfo(service.UserID)
		if err != nil {
			log.Error("GetUserSSInfo failed:", service.UserID)
			continue
		}
		ssModel.UpdateUserService(service.ID, ssModel.ServiceStatusRunning, service.FlowUsed)
		ssm.StartTunnels(ssinfo, nodes)
	}
}

func loopFlows() {
	t := time.NewTicker(1 * time.Minute)
	for range t.C {
		log.Info("running Flow...")
		go runFlows()
	}
}

func loopFix() {
	ssm.FixTunnels()
	t := time.NewTicker(time.Hour)
	for range t.C {
		go ssm.FixTunnels()
	}
}

func run() {
	/* 启动的时候检查一下代理是否正常 */
	go loopFix()
	go loopFlows()
}
