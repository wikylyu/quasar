package product

import "time"

const (
	StatusOK        = 0
	StatusInvisible = 1
)

type Product struct {
	TableName         struct{}  `sql:"product" json:"-"`
	Id                string    `sql:"id,pk" json:"id"`
	Name              string    `sql:"name" json:"name"`
	Price             uint64    `sql:"price" json:"price"`
	NumberOfMonths    uint      `sql:"number_of_months" json:"number_of_months"`
	FlowLimitPerMonth uint64    `sql:"flow_limit_per_month" json:"flow_limit_per_month"`
	Status            int       `sql:"status" json:"status"`
	Sort              int       `sql:"sort" json:"-"`
	CTime             time.Time `sql:"ctime,null" json:"ctime"`
	UTime             time.Time `sql:"utime,null" json:"utime"`
}
