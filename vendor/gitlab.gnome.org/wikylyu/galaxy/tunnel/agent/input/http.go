/*
 * Copyright (C) 2018 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package input

import (
	"bufio"
	"bytes"
	"fmt"
	"net"
	"net/http"
	"strconv"

	"gitlab.gnome.org/wikylyu/galaxy/gnet"
	"gitlab.gnome.org/wikylyu/galaxy/tunnel/agent"
)

type HttpFactory struct {
	username string
	password string
}

func (*HttpFactory) Name() string {
	return "http"
}

func (f *HttpFactory) Setup(options agent.Options) error {
	f.username = options.GetString("username")
	f.password = options.GetString("password")
	return nil
}

func (f *HttpFactory) Options() map[string]string {
	m := make(map[string]string)
	if f.username != "" {
		m["username"] = f.username
	}
	if f.password != "" {
		m["password"] = f.password
	}
	return m
}

func (f *HttpFactory) NewIAgent(gc *gnet.GConn) (agent.IAgent, error) {
	a := HttpAgent{
		GConn:    gc,
		username: f.username,
		password: f.password,
		req:      nil,
	}
	return &a, nil
}

type HttpAgent struct {
	*gnet.GConn
	username string
	password string

	req *http.Request
}

func (a *HttpAgent) NeedAuth() bool {
	return a.username != "" && a.password != ""
}

func (a *HttpAgent) Auth(username, password string) bool {
	return a.username == username && a.password == password
}

func (a *HttpAgent) Init() (string, uint16, error) {
	r := bufio.NewReader(a.GConn.Conn)
	req, err := http.ReadRequest(r)
	if err != nil {
		return "", 0, err
	}
	if a.NeedAuth() {
		if u, p, _ := req.BasicAuth(); !a.Auth(u, p) {
			return "", 0, fmt.Errorf("Invalid Auth")
		}
	}
	a.req = req
	return a.getHostPort()
}

func (a *HttpAgent) getHostPort() (string, uint16, error) {
	if a.req.Host != "" {
		return getRequestHostPort(a.req.Host)
	} else if a.req.URL.Host == "" {
		return "", 0, fmt.Errorf("Invalid Host")
	}
	port := uint16(0)
	if a.req.URL.Port() == "" {
		if a.req.URL.Scheme == "https" {
			port = 443
		} else if a.req.URL.Scheme == "" || a.req.URL.Scheme == "http" {
			port = 80
		} else {
			return "", 0, fmt.Errorf("Invalid Port")
		}
	} else if p, err := strconv.Atoi(a.req.URL.Port()); err != nil {
		port = uint16(p)
	} else {
		return "", 0, fmt.Errorf("Invalid Port")
	}
	return a.req.URL.Host, port, nil
}

func (a *HttpAgent) Notify(err error) error {
	if a.req.Method != http.MethodConnect {
		return nil
	}
	resp := http.Response{
		Status:     "200 Connection Established",
		StatusCode: 200,
		Proto:      "HTTP/1.1",
		ProtoMajor: 1,
		ProtoMinor: 1,
	}
	if err != nil {
		resp.Status = "503 Service Unavailable"
		resp.StatusCode = 503
	}
	a.req = nil
	return resp.Write(a.GConn.Conn)
}

func (a *HttpAgent) Read() ([]byte, error) {
	if a.req != nil {
		buf := new(bytes.Buffer)
		if err := a.req.Write(buf); err != nil {
			return nil, err
		}
		a.req = nil
		return buf.Bytes(), nil
	}
	return a.GConn.Read()
}

func getRequestHostPort(s string) (string, uint16, error) {
	host, port, err := net.SplitHostPort(s)
	if err != nil {
		return s, 80, nil
	}
	p, err := strconv.Atoi(port)
	if err != nil {
		return "", 0, err
	}
	return host, uint16(p), nil
}
