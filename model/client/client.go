package client

import (
	log "github.com/sirupsen/logrus"
	"gitlab.gnome.org/wikylyu/quasar/errors"
	"gitlab.gnome.org/wikylyu/quasar/model/db"
)

func GetClient(clientID string) (*Client, error) {
	conn := db.PG()

	client := &Client{
		ID: clientID,
	}
	if err := conn.Select(client); err != nil {
		if err == db.ErrNoRows {
			return nil, nil
		}
		log.Error("SQL Error:", err)
		return nil, errors.ErrDB
	}
	return client, nil
}
