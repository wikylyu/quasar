/*
 * Copyright (C) 2018 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package main

import (
	"fmt"

	"github.com/abiosoft/ishell"
	"github.com/spf13/pflag"
	"gitlab.gnome.org/wikylyu/galaxy/cmd"
	"gitlab.gnome.org/wikylyu/galaxy/rpc"
)

func main() {
	var host, username, password string
	var port uint16
	var insecure bool
	pflag.StringVarP(&host, "host", "h", "127.0.0.1", "host")
	pflag.Uint16VarP(&port, "port", "P", 12121, "port")
	pflag.StringVarP(&username, "user", "u", "", "username")
	pflag.StringVarP(&password, "password", "p", "", "password")
	pflag.BoolVarP(&insecure, "without-tls", "i", false, "disable tls")
	pflag.Parse()

	gc, err := rpc.NewGravityClient(fmt.Sprintf("%s:%d", host, port), !insecure, username, password)
	if err != nil {
		panic(err)
	}

	shell := ishell.New()
	shell.SetPrompt("\x1B[36mgravity>>\x1B[0m ")
	shell.SetHomeHistoryPath(".gravity_history")
	shell.Set("gc", gc)

	shell.AddCmd(&cmd.VERSION)
	shell.AddCmd(&cmd.LIST)
	shell.AddCmd(&cmd.START)
	shell.AddCmd(&cmd.STOP)
	shell.AddCmd(&cmd.ADD)
	shell.AddCmd(&cmd.UPDATE)
	shell.AddCmd(&cmd.REMOVE)
	shell.AddCmd(&cmd.STAT)
	shell.AddCmd(&cmd.LOAD)

	shell.Run()
}
