package account

import (
	"gitlab.gnome.org/wikylyu/quasar/errors"
	"gitlab.gnome.org/wikylyu/quasar/model/db"

	log "github.com/sirupsen/logrus"
	"gitlab.gnome.org/wikylyu/quasar/util"
)

/* 创建帐号 */
func CreateAccount(email, password, ptype string) (*Account, error) {
	conn := db.PG()

	tx, err := conn.Begin()
	if err != nil {
		return nil, errors.ErrDB
	}
	defer tx.Rollback()

	a := &Account{
		Email:  email,
		UType:  UTypeCustomer,
		Status: StatusOK,
	}
	if err := tx.Model(a).Where(`email = ?`, email).Select(); err != nil {
		if err != db.ErrNoRows {
			log.Error("SQL Error:", err)
			return nil, errors.ErrDB
		}
	} else {
		return nil, errors.ErrEmailExists
	}

	salt, err := util.RandString(12)
	if err != nil {
		log.Error("RandString error:", err)
		return nil, err
	}

	password, ptype = encryptPassword(salt, password, ptype)
	a.Salt = salt
	a.Password = password
	a.PType = ptype
	if err := tx.Insert(a); err != nil {
		log.Error("SQL Error:", err)
		return nil, err
	}

	up := &UserProfile{
		UserID: a.ID,
	}
	if err := tx.Insert(up); err != nil {
		log.Error("SQL Error:", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Error("SQL Commit failed:", err)
		return nil, err
	}
	return a, nil
}

func GetAccount(accountID string) (*Account, error) {
	conn := db.PG()

	a := Account{
		ID: accountID,
	}
	if err := conn.Select(&a); err != nil {
		if err != db.ErrNoRows {
			log.Error("SQL Error:", err)
			return nil, errors.ErrDB
		}
		return nil, nil
	}
	return &a, nil
}

func GetAccountByEmail(email string) (*Account, error) {
	conn := db.PG()

	a := Account{}
	if err := conn.Model(&a).Where("email = ?", email).Select(); err != nil {
		if err != db.ErrNoRows {
			log.Error("SQL Error:", err)
			return nil, errors.ErrDB
		}
		return nil, nil
	}
	return &a, nil
}
