package ssm

import (
	"sync"

	log "github.com/sirupsen/logrus"
	"gitlab.gnome.org/wikylyu/galaxy/rpc"
	ssModel "gitlab.gnome.org/wikylyu/quasar/model/ss"
)

var clients map[string]*rpc.GravityClient = make(map[string]*rpc.GravityClient)
var clientMutex sync.Mutex

func getGravityClient(n *ssModel.Node) *rpc.GravityClient {
	defer clientMutex.Unlock()
	clientMutex.Lock()

	if c := clients[n.ID]; c != nil {
		return c
	}
	c, err := rpc.NewGravityClient(n.String(), true, n.GravityUsername, n.GravityPassword)
	if err != nil {
		log.Error("NewGravityClient error:", err)
		return nil
	}
	clients[n.ID] = c
	return c
}
