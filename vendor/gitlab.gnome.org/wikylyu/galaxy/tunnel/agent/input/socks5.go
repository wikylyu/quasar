/*
 * Copyright (C) 2018 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package input

import (
	"fmt"

	"gitlab.gnome.org/wikylyu/galaxy/gnet"
	"gitlab.gnome.org/wikylyu/galaxy/protocol/socks"
	"gitlab.gnome.org/wikylyu/galaxy/tunnel/agent"
)

type Socks5Factory struct {
	username string
	password string
}

func (*Socks5Factory) Name() string {
	return "socks5"
}

func (f *Socks5Factory) Setup(options agent.Options) error {
	f.username = options.GetString("username")
	f.password = options.GetString("password")
	return nil
}

func (f *Socks5Factory) Options() map[string]string {
	m := make(map[string]string)
	if f.username != "" {
		m["username"] = f.username
	}
	if f.password != "" {
		m["password"] = f.password
	}
	return m
}

func (f *Socks5Factory) NewIAgent(gc *gnet.GConn) (agent.IAgent, error) {
	a := Socks5Agent{
		GConn:    gc,
		username: f.username,
		password: f.password,
	}
	return &a, nil
}

/* Socks5 协议 */
type Socks5Agent struct {
	*gnet.GConn
	username string
	password string

	request *socks.Socks5Request
}

func (a *Socks5Agent) NeedAuth() bool {
	return a.username != "" && a.password != ""
}

func (a *Socks5Agent) Auth(username, password string) bool {
	return a.username == username && a.password == password
}

func (a *Socks5Agent) doMethodSelection() (byte, error) {
	buf, err := a.Read()
	if err != nil {
		return 0, err
	}
	req, err := socks.ParseMethodSelectionRequest(buf)
	if err != nil {
		return 0, err
	} else if req.VER != socks.Version5 {
		return 0, fmt.Errorf("Invalid Version %d", req.VER)
	}
	method := socks.MethodNoAuthRequired
	if a.NeedAuth() {
		method = socks.MethodUsernamePassword
	}
	rep := socks.NewMethodSelectionReply(socks.Version5, socks.MethodNoAcceptable)
	for _, m := range req.METHODS {
		if m == method {
			rep.METHOD = method
			break
		}
	}
	if err := a.Write(rep.Build()); err != nil {
		return 0, err
	} else if rep.METHOD == socks.MethodNoAcceptable {
		return 0, fmt.Errorf("No Acceptable Method")
	}
	return rep.METHOD, nil
}

func (a *Socks5Agent) doUsernamePassword() error {
	buf, err := a.Read()
	if err != nil {
		return err
	}
	req, err := socks.ParseUsernamePasswordRequest(buf)
	if err != nil {
		return err
	}
	status := socks.UsernamePasswordStatusSuccess
	passed := a.Auth(req.UNAME, req.PASSWD)
	if !passed {
		status = socks.UsernamePasswordStatusFailure
	}
	rep := socks.NewUsernamePasswordReply(socks.Version5, status)
	if err := a.Write(rep.Build()); err != nil {
		return err
	} else if !passed {
		return fmt.Errorf("Invalid Username/Password")
	}
	return nil
}

func (a *Socks5Agent) doCMDRequest() (string, uint16, error) {
	buf, err := a.Read()
	if err != nil {
		return "", 0, err
	}
	req, err := socks.ParseSocks5Request(buf)
	if err != nil {
		return "", 0, err
	}
	if req.CMD == socks.CMDConnect {
		a.request = req
	} else {
		return "", 0, fmt.Errorf("Invalid Command %d", req.CMD)
	}
	return req.ADDR, req.PORT, nil
}

/* 执行SOCKS5协议的初始化过程 */
func (a *Socks5Agent) Init() (string, uint16, error) {
	if method, err := a.doMethodSelection(); err != nil {
		return "", 0, err
	} else if method == socks.MethodUsernamePassword {
		if err := a.doUsernamePassword(); err != nil {
			return "", 0, err
		}
	}
	return a.doCMDRequest()
}

func (a *Socks5Agent) Notify(err error) error {
	status := socks.ReplySuccess
	if err != nil {
		status = socks.ReplyGeneralFailure
	}
	rep := socks.NewSocks5Reply(socks.Version5, status, a.request.ATYP, a.request.ADDR, a.request.PORT)
	if err := a.Write(rep.Build()); err != nil {
		return err
	}
	return nil
}
