package order

import (
	"net/http"

	"gitlab.gnome.org/wikylyu/quasar/controller/context"
	"gitlab.gnome.org/wikylyu/quasar/errors"
	orderModel "gitlab.gnome.org/wikylyu/quasar/model/order"

	"github.com/labstack/echo"
)

/* 创建订单 */
func CreateOrderFromProducts(c echo.Context) error {
	ctx := c.(*context.Context)
	data := CreateOrderFromProductRequest{}
	if err := ctx.Bind(&data); err != nil {
		return err
	} else if err := ctx.Validate(&data); err != nil {
		return err
	}
	if len(data.ProductIds) == 0 {
		return errors.ErrProductNotFound
	}
	order, err := orderModel.CreateOrderFromProducts(ctx.Account.ID, data.ProductIds...)
	if err != nil {
		return err
	}
	return ctx.JSON(http.StatusOK, order)
}

/* 获取订单信息 */
func GetOrder(c echo.Context) error {
	ctx := c.(*context.Context)
	orderID := c.Param("id")
	order, err := orderModel.GetOrder(orderID)
	if err != nil {
		return err
	} else if order == nil {
		return errors.ErrOrderNotFound
	}
	return ctx.JSON(http.StatusOK, order)
}

/* 获取订单的支付信息 */
func GetOrderPayInfo(c echo.Context) error {
	ctx := c.(*context.Context)
	orderID := c.Param("id")
	order, err := orderModel.PayOrder(orderID)
	if err != nil {
		return err
	} else if order == nil {
		return errors.ErrOrderNotFound
	}
	return ctx.JSON(http.StatusOK, order)
}
