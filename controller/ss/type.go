package ss

const (
	FlowPeriodHourly = "hourly"
	FlowPeriodDaily = "daily"
	FlowPeriodMonthly = "monthly"
)

type FindUserFlowStatsRequest struct {
	Period string  `json:"period" form:"period" query:"period"`
	Hours int `json:"hours" form:"hours" query:"hours"`
	Days int `json:"days" form:"days" query:"days"`
	Months int `json:"months" form:"months" query:"months"`
}
