package context

import (
	"github.com/labstack/echo"
	accountModel "gitlab.gnome.org/wikylyu/quasar/model/account"
	clientModel "gitlab.gnome.org/wikylyu/quasar/model/client"
)

type Context struct {
	echo.Context
	Token   *accountModel.Token
	Client  *clientModel.Client
	Account *accountModel.Account
}
