/*
 * Copyright (C) 2015 - 2017 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package cipher

import (
	"strings"
)

type newEncrypterFunc func([]byte, []byte) (Encrypter, error)
type newDecrypterFunc func([]byte, []byte) (Decrypter, error)

type Encrypter interface {
	Encrypt([]byte) []byte
}

type Decrypter interface {
	Decrypt([]byte) []byte
}

type CipherInfo struct {
	KeySize       int
	IvSize        int
	TagSize       int
	EncrypterFunc newEncrypterFunc
	DecrypterFunc newDecrypterFunc
	Block         bool
}

var (
	cipherInfos = map[string]*CipherInfo{
		"aes-128-cfb": {16, 16, 0, newAESCFBEncrypter, newAESCFBDecrypter, false},
		"aes-192-cfb": {24, 16, 0, newAESCFBEncrypter, newAESCFBDecrypter, false},
		"aes-256-cfb": {32, 16, 0, newAESCFBEncrypter, newAESCFBDecrypter, false},
		"rc4-md5":     {16, 16, 0, newRC4MD5Encrypter, newRC4MD5Decrypter, false},
		"none":        {0, 0, 0, newNoneEncrypter, newNoneDecrypter, false},
		"not":         {0, 0, 0, newNotEncrypter, newNotDecrypter, false},
		"aes-128-gcm": {16, 16, 16, newAESGCMEncrypter, newAESGCMDecrypter, true},
		"aes-192-gcm": {24, 24, 16, newAESGCMEncrypter, newAESGCMDecrypter, true},
		"aes-256-gcm": {32, 32, 16, newAESGCMEncrypter, newAESGCMDecrypter, true},
	}
)

func GetCipherInfo(name string) *CipherInfo {
	info := cipherInfos[strings.ToLower(name)]
	return info
}

func GetAllCipherInfos() map[string]*CipherInfo {
	return cipherInfos
}
