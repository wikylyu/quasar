package ss

import (
	"fmt"
	"time"
)

const (
	ServiceStatusWaiting  = 0
	ServiceStatusRunning  = 1
	ServiceStatusFinished = 2
	ServiceStatusLimited  = 3
)

type Node struct {
	TableName       struct{}  `sql:"node" json:"-"`
	ID              string    `sql:"id" json:"id"`
	Name            string    `sql:"name" json:"name"`
	Description     string    `sql:"description" json:"description"`
	PublicHost      string    `sql:"public_host" json:"public_host"`
	GravityAddr     string    `sql:"gravity_addr" json:"-"`
	GravityPort     uint16    `sql:"gravity_port" json:"-"`
	GravityUsername string    `sql:"gravity_username" json:"-"`
	GravityPassword string    `sql:"gravity_password" json:"-"`
	UTime           time.Time `sql:"utime,null" json:"utime"`
	CTime           time.Time `sql:"ctime,null" json:"ctime"`
}

func (n *Node) String() string {
	return fmt.Sprintf("%s:%d", n.GravityAddr, n.GravityPort)
}

type UserService struct {
	TableName struct{}  `sql:"user_service" json:"-"`
	ID        string    `sql:"id,pk" json:"id"`
	UserID    string    `sql:"user_id" json:"user_id"`
	StartTime time.Time `sql:"starttime" json:"starttime"`
	EndTime   time.Time `sql:"endtime" json:"endtime"`
	FlowLimit uint64    `sql:"flow_limit" json:"flow_limit"`
	FlowUsed  uint64    `sql:"flow_used,notnull" json:"flow_used"`
	Status    int       `sql:"status,notnull" json:"status"`
	CTime     time.Time `sql:"ctime,null" json:"ctime"`
	UTime     time.Time `sql:"utime,null" json:"utime"`
}

type UserSSInfo struct {
	TableName struct{}  `sql:"user_ss_info" json:"-"`
	UserID    string    `sql:"user_id,pk" json:"user_id"`
	Port      uint16    `sql:"port,null" json:"port"`
	Password  string    `sql:"passwd" json:"password"`
	Method    string    `sql:"method" json:"method"`
	CTime     time.Time `sql:"ctime,null" json:"ctime"`
	UTime     time.Time `sql:"utime,null" json:"utime"`
}

/* 根据当前时间和流量使用情况更新服务状态 */
func (us *UserService) UpdateStatus(now time.Time) {
	status := us.Status
	if (now.Equal(us.StartTime) || now.After(us.StartTime)) && now.Before(us.EndTime) {
		status = ServiceStatusRunning
		if us.FlowLimit <= us.FlowUsed {
			status = ServiceStatusLimited
		}
	} else if now.After(us.EndTime) || now.Equal(us.EndTime) {
		status = ServiceStatusFinished
	}
	us.Status = status
}

type UserNodeMonthlyFlowStat struct {
	TableName struct{}  `sql:"user_node_monthly_flow_stat" json:"-"`
	UserID    string    `sql:"user_id" json:"user_id"`
	NodeID    string    `sql:"node_id" json:"node_id"`
	MTime     time.Time `sql:"mtime" json:"mtime"`
	Year      int       `sql:"-" json:"year"`
	Month     int       `sql:"-" json:"month"`
	Ingress   uint64    `sql:"ingress,notnull" json:"ingress"`
	Egress    uint64    `sql:"egress,notnull" json:"egress"`
	CTime     time.Time `sql:"ctime,null" json:"ctime"`
	UTime     time.Time `sql:"utime,null" json:"utime"`
}

type UserMonthlyFlowStat struct {
	TableName struct{}  `sql:"user_monthly_flow_stat" json:"-"`
	UserID    string    `sql:"user_id" json:"user_id"`
	MTime     time.Time `sql:"mtime" json:"mtime"`
	Year      int       `sql:"-" json:"year"`
	Month     int       `sql:"-" json:"month"`
	Ingress   uint64    `sql:"ingress,notnull" json:"ingress"`
	Egress    uint64    `sql:"egress,notnull" json:"egress"`
	CTime     time.Time `sql:"ctime,null" json:"ctime"`
	UTime     time.Time `sql:"utime,null" json:"utime"`
}

type UserNodeDailyFlowStat struct {
	TableName struct{}  `sql:"user_node_daily_flow_stat" json:"-"`
	UserID    string    `sql:"user_id" json:"user_id"`
	NodeID    string    `sql:"node_id" json:"node_id"`
	MTime     time.Time `sql:"mtime" json:"mtime"`
	Year      int       `sql:"-" json:"year"`
	Month     int       `sql:"-" json:"month"`
	Day       int       `sql:"-" json:"day"`
	Ingress   uint64    `sql:"ingress,notnull" json:"ingress"`
	Egress    uint64    `sql:"egress,notnull" json:"egress"`
	CTime     time.Time `sql:"ctime,null" json:"ctime"`
	UTime     time.Time `sql:"utime,null" json:"utime"`
}

type UserDailyFlowStat struct {
	TableName struct{}  `sql:"user_daily_flow_stat" json:"-"`
	UserID    string    `sql:"user_id" json:"user_id"`
	MTime     time.Time `sql:"mtime" json:"mtime"`
	Year      int       `sql:"-" json:"year"`
	Month     int       `sql:"-" json:"month"`
	Day       int       `sql:"-" json:"day"`
	Ingress   uint64    `sql:"ingress,notnull" json:"ingress"`
	Egress    uint64    `sql:"egress,notnull" json:"egress"`
	CTime     time.Time `sql:"ctime,null" json:"ctime"`
	UTime     time.Time `sql:"utime,null" json:"utime"`
}

type UserNodeHourlyFlowStat struct {
	TableName struct{}  `sql:"user_node_hourly_flow_stat" json:"-"`
	UserID    string    `sql:"user_id" json:"user_id"`
	NodeID    string    `sql:"node_id" json:"node_id"`
	MTime     time.Time `sql:"mtime" json:"mtime"`
	Year      int       `sql:"-" json:"year"`
	Month     int       `sql:"-" json:"month"`
	Day       int       `sql:"-" json:"day"`
	Hour      int       `sql:"-" json:"hour"`
	Ingress   uint64    `sql:"ingress,notnull" json:"ingress"`
	Egress    uint64    `sql:"egress,notnull" json:"egress"`
	CTime     time.Time `sql:"ctime,null" json:"ctime"`
	UTime     time.Time `sql:"utime,null" json:"utime"`
}

type UserHourlyFlowStat struct {
	TableName struct{}  `sql:"user_hourly_flow_stat" json:"-"`
	UserID    string    `sql:"user_id" json:"user_id"`
	MTime     time.Time `sql:"mtime" json:"mtime"`
	Year      int       `sql:"-" json:"year"`
	Month     int       `sql:"-" json:"month"`
	Day       int       `sql:"-" json:"day"`
	Hour      int       `sql:"-" json:"hour"`
	Ingress   uint64    `sql:"ingress,notnull" json:"ingress"`
	Egress    uint64    `sql:"egress,notnull" json:"egress"`
	CTime     time.Time `sql:"ctime,null" json:"ctime"`
	UTime     time.Time `sql:"utime,null" json:"utime"`
}
