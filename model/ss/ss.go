package ss

import (
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.gnome.org/wikylyu/quasar/errors"
	"gitlab.gnome.org/wikylyu/quasar/model/db"
	"gitlab.gnome.org/wikylyu/quasar/util"
)

func FindNodes() ([]*Node, error) {
	conn := db.PG()

	nodes := make([]*Node, 0)
	if err := conn.Model(&nodes).Order("ctime").Select(); err != nil {
		log.Error("SQL Error:", err)
		return nil, errors.ErrDB
	}
	return nodes, nil
}

func FindUserSSInfos() ([]*UserSSInfo, error) {
	conn := db.PG()

	infos := make([]*UserSSInfo, 0)
	if err := conn.Model(&infos).Select(); err != nil {
		log.Error("SQL Error:", err)
		return nil, errors.ErrDB
	} 
	return infos, nil
}

func GetUserSSInfo(userID string) (*UserSSInfo, error) {
	conn := db.PG()

	ssinfo := UserSSInfo{
		UserID: userID,
	}
	if err := conn.Select(&ssinfo); err != nil {
		if err != db.ErrNoRows {
			log.Error("SQL Error:", err)
			return nil, errors.ErrDB
		}
		return nil, nil
	}
	return &ssinfo, nil
}

func GetCurrentUserService(userID string) (*UserService, error) {
	conn := db.PG()

	us := UserService{}
	if err := conn.Model(&us).Where("user_id=? AND starttime<=CURRENT_TIMESTAMP AND endtime>CURRENT_TIMESTAMP", userID).Limit(1).Select(); err != nil {
		if err != db.ErrNoRows {
			log.Error("SQL Error:", err)
			return nil, errors.ErrDB
		}
		return nil, nil
	}
	return &us, nil
}

/* 返回是否是第一个服务 */
func AddUserService(userID string, numberOfMonths uint, flowLimitPerMonth uint64) (bool, error) {
	conn := db.PG()

	newSS := false

	tx, err := conn.Begin()
	if err != nil {
		log.Error("SQL Error:", err)
		return false, errors.ErrDB
	}
	defer tx.Rollback()

	ssinfo := UserSSInfo{
		UserID: userID,
	}

	if err := tx.Select(&ssinfo); err != nil {
		if err != db.ErrNoRows {
			log.Error("SQL Error:", err)
			return false, errors.ErrDB
		}
		newSS = true
		ssinfo.Password, _ = util.RandString(12)
		ssinfo.Method = "aes-256-cfb"
		if _, err := tx.Model(&ssinfo).Returning("*").Insert(); err != nil {
			log.Error("SQL Error:", err)
			return false, errors.ErrDB
		}
	}
	var lastService UserService
	var startTime, endTime time.Time
	if err := tx.Model(&lastService).Where("user_id=?", userID).Order("starttime DESC").Limit(1).Select(); err != nil {
		if err != db.ErrNoRows {
			log.Error("SQL Error:", err)
			return false, errors.ErrDB
		}
		startTime = time.Now()
	} else if lastService.StartTime.After(time.Now()) {
		startTime = lastService.StartTime
	} else {
		startTime = time.Now()
	}
	for i := uint(0); i < numberOfMonths; i++ {
		endTime = startTime.AddDate(0, 1, 0)
		us := UserService{
			UserID:    userID,
			StartTime: startTime,
			EndTime:   endTime,
			FlowLimit: flowLimitPerMonth,
			FlowUsed:  0,
		}
		us.UpdateStatus(time.Now())
		if err := tx.Insert(&us); err != nil {
			log.Error("SQL Error:", err)
			return false, errors.ErrDB
		}

		startTime = endTime
	}

	if err := tx.Commit(); err != nil {
		log.Error("SQL Error:", err)
		return false, errors.ErrDB
	}
	return newSS, nil
}

/* 获取所有用户当前的有效服务 */
func FindUserServiceWithStatus(status int) ([]*UserService, error) {
	conn := db.PG()

	services := make([]*UserService, 0)
	if err := conn.Model(&services).Where("status=?", status).Select(); err != nil {
		log.Error("SQL Error:", err)
		return nil, errors.ErrDB
	}
	return services, nil
}

func FindUserServiceWithTime(t time.Time, statut int) ([]*UserService, error) {
	conn := db.PG()
	services := make([]*UserService, 0)
	if err := conn.Model(&services).Where("status=? AND starttime<=? AND endtime>?", statut, t, t).Select(); err != nil {
		log.Error("SQL Error:", err)
		return nil, errors.ErrDB
	}
	return services, nil
}

func UpdateUserService(serviceID string, status int, flowUsed uint64) error {
	conn := db.PG()
	service := UserService{
		ID:       serviceID,
		Status:   status,
		FlowUsed: flowUsed,
	}
	if _, err := conn.Model(&service).Column("status", "flow_used").WherePK().Update(); err != nil {
		log.Error("SQL Error:", err)
		return errors.ErrDB
	}
	return nil
}
