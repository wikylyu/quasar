/*
 * Copyright (C) 2015 - 2017 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package cipher

import (
	"crypto/aes"
	"crypto/cipher"
)

/* AES CFB模式 加密 */
type aesCFBEncrypter struct {
	stream cipher.Stream
}

func (e *aesCFBEncrypter) Encrypt(plain []byte) []byte {
	return cipherStreamXOR(e.stream, plain)
}

func newAESCFBEncrypter(key, iv []byte) (Encrypter, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	return &aesCFBEncrypter{stream}, nil
}

/* AES CFB模式 解密 */
type aesCFBDecrypter struct {
	stream cipher.Stream
}

func (e *aesCFBDecrypter) Decrypt(encrypted []byte) []byte {
	return cipherStreamXOR(e.stream, encrypted)
}

func newAESCFBDecrypter(key, iv []byte) (Decrypter, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	stream := cipher.NewCFBDecrypter(block, iv)
	return &aesCFBDecrypter{stream}, nil
}

type aesGCMEncrypter struct {
	stream cipher.AEAD
	nonce  []byte
}

func (e *aesGCMEncrypter) Encrypt(plain []byte) []byte {
	ciphertext := e.stream.Seal(nil, e.nonce, plain, nil)
	incNonce(e.nonce)
	return ciphertext
}

func newAESGCMEncrypter(key, salt []byte) (Encrypter, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	return &aesGCMEncrypter{
		stream: aesgcm,
		nonce:  make([]byte, 12),
	}, nil
}

type aesGCMDecrypter struct {
	stream cipher.AEAD
	nonce  []byte
}

func (e *aesGCMDecrypter) Decrypt(ciphertext []byte) []byte {
	plaintext, err := e.stream.Open(nil, e.nonce, ciphertext, nil)
	if err != nil {
		return nil
	}
	incNonce(e.nonce)
	return plaintext
}

func newAESGCMDecrypter(subkey, salt []byte) (Decrypter, error) {
	block, err := aes.NewCipher(subkey)
	if err != nil {
		return nil, err
	}

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	return &aesGCMDecrypter{
		stream: aesgcm,
		nonce:  make([]byte, 12),
	}, nil
}

func incNonce(b []byte) {
	for i := range b {
		b[i]++
		if b[i] != 0 {
			return
		}
	}
}
