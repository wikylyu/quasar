package account

const (
	KeyAccount = "quasar.account"
	KeyToken   = "quasar.token"
)

func getTokenKey(tokenID string) string {
	return KeyToken + "." + tokenID
}

func getAccountKey(userID string) string {
	return KeyAccount + "." + userID
}
