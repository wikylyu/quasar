package errors

import (
	"fmt"
	"net/http"
)

/* 这里的错误全部都是对客户端的 */
var (
	ErrDB            = NewError(http.StatusInternalServerError, -1, "数据库错误")
	ErrClientInvalid = NewError(http.StatusBadRequest, -2, "客户端未定义")
	ErrParamsInvalid = NewError(http.StatusBadRequest, -3, "无效的参数")

	ErrTokenExpired      = NewError(http.StatusUnauthorized, 10, "token已过期")
	ErrEmailExists       = NewError(http.StatusBadRequest, 100, "邮箱已存在")
	ErrUserNotFound      = NewError(http.StatusNotFound, 101, "用户不存在")
	ErrPasswordIncorrect = NewError(http.StatusBadRequest, 102, "密码错误")
	ErrPasswordInvalid   = NewError(http.StatusBadRequest, 103, "密码格式不合法")
	ErrEmailInvalid      = NewError(http.StatusBadRequest, 104, "邮箱格式不合法")

	ErrNoSSInfo           = NewError(http.StatusNotFound, 200, "还未购买任何服务")
	ErrNoService          = NewError(http.StatusNotFound, 201, "服务不存在")
	ErrProductNotFound    = NewError(http.StatusNotFound, 202, "商品不存在")
	ErrOrderNotFound      = NewError(http.StatusNotFound, 203, "订单不存在")
	ErrOrderStatusInvalid = NewError(http.StatusTeapot, 204, "订单状态不合法")

	ErrFileNotFound = NewError(http.StatusNotFound, 1000, "文件不存在")
)

func NewError(status, code int, msg interface{}) *Error {
	return &Error{
		HttpStatus: status,
		Code:       code,
		Message:    fmt.Sprintf("%v", msg),
	}
}

func CopyError(e *Error) *Error {
	err := new(Error)
	*err = *e
	return err
}

func CopyErrorWithMsg(e *Error, msg string) *Error {
	err := CopyError(e)
	err.Message = msg
	return err
}

type Error struct {
	HttpStatus int    `json:"-"`
	Code       int    `json:"code"`
	Message    string `json:"message"`
}

func (e *Error) Error() string {
	return e.Message
}
