package order

import (
	log "github.com/sirupsen/logrus"
	"gitlab.gnome.org/wikylyu/quasar/errors"
	"gitlab.gnome.org/wikylyu/quasar/model/db"
	productModel "gitlab.gnome.org/wikylyu/quasar/model/product"
	ssModel "gitlab.gnome.org/wikylyu/quasar/model/ss"
	"gitlab.gnome.org/wikylyu/quasar/ssm"
)

func GetOrder(orderID string) (*Order, error) {
	conn := db.PG()

	order := Order{
		ID: orderID,
	}
	if err := conn.Select(&order); err != nil {
		if err != db.ErrNoRows {
			log.Error("SQL Error:", err)
			return nil, errors.ErrDB
		}
		return nil, nil
	}
	return &order, nil
}

/* 创建订单 */
func CreateOrderFromProducts(userId string, productIds ...string) (*Order, error) {
	conn := db.PG()

	tx, err := conn.Begin()
	if err != nil {
		log.Error("SQL Error:", err)
		return nil, errors.ErrDB
	}
	defer tx.Rollback()
	var price uint64
	var products []*productModel.Product
	for _, productId := range productIds {
		product := &productModel.Product{
			Id: productId,
		}
		if err := tx.Select(product); err != nil {
			if err != db.ErrNoRows {
				log.Error("SQL Error:", err)
				return nil, errors.ErrDB
			}
			return nil, errors.ErrProductNotFound
		} else if product.Status != productModel.StatusOK {
			return nil, errors.ErrProductNotFound
		}
		products = append(products, product)
		price += product.Price
	}
	order := Order{
		Price:  price,
		UserID: userId,
		Status: StatusUnpaid,
	}
	if err := tx.Insert(&order); err != nil {
		log.Error("SQL Error:", err)
		return nil, errors.ErrDB
	}
	for _, product := range products {
		orderItem := OrderItem{
			OrderID:   order.ID,
			ProductID: product.Id,
			UserId:    userId,
		}
		if err := tx.Insert(&orderItem); err != nil {
			log.Error("SQL Error:", err)
			return nil, errors.ErrDB
		}
		orderItemSnapshot := OrderItemSnapshot{
			OrderItemID:       orderItem.ID,
			Name:              product.Name,
			Price:             product.Price,
			NumberOfMonths:    product.NumberOfMonths,
			FlowLimitPerMonth: product.FlowLimitPerMonth,
		}
		if err := tx.Insert(&orderItemSnapshot); err != nil {
			log.Error("SQL Error:", err)
			return nil, errors.ErrDB
		}
	}

	if err := tx.Commit(); err != nil {
		log.Error("SQL Error:", err)
		return nil, errors.ErrDB
	}
	return &order, nil
}

/* 支付订单，设置用户的订单为已支付，并设置相应的服务信息 */
func PayOrder(orderID string) (*Order, error) {
	conn := db.PG()

	tx, err := conn.Begin()
	if err != nil {
		log.Error("SQL Error:", err)
		return nil, errors.ErrDB
	}
	defer tx.Rollback()

	order := Order{ID: orderID}
	if err := tx.Select(&order); err != nil {
		if err != db.ErrNoRows {
			log.Error("SQL Error:", err)
			return nil, errors.ErrDB
		}
		return nil, errors.ErrOrderNotFound
	}
	if order.Status != StatusUnpaid {
		return nil, errors.ErrOrderStatusInvalid
	}
	order.Status = StatusPaid
	if _, err := tx.Model(&order).WherePK().Column("status").Update(); err != nil {
		log.Error("SQL Error:", err)
		return nil, errors.ErrDB
	}

	items := make([]*OrderItem, 0)
	if err := tx.Model(&items).Where("order_id = ?", order.ID).Select(); err != nil {
		log.Error("SQL Error:", err)
		return nil, errors.ErrDB
	}
	for _, item := range items {
		var snapshot OrderItemSnapshot
		if err := tx.Model(&snapshot).Where("order_item_id = ?", item.ID).Select(); err != nil {
			log.Error("SQL Error:", err)
			return nil, errors.ErrDB
		}
		if newSS, err := ssModel.AddUserService(order.UserID, snapshot.NumberOfMonths, snapshot.FlowLimitPerMonth); err != nil {
			return nil, err
		} else if newSS {
			addTunnels(order.UserID)
		}
	}

	if err := tx.Commit(); err != nil {
		log.Error("SQL Error:", err)
		return nil, errors.ErrDB
	}

	startTunnels(order.UserID)
	return &order, nil
}

func addTunnels(userID string) {
	ssinfo, err := ssModel.GetUserSSInfo(userID)
	if err != nil || ssinfo == nil {
		return
	}
	nodes, err := ssModel.FindNodes()
	if err != nil {
		return
	}
	ssm.AddTunnels(ssinfo, nodes)
}

func startTunnels(userID string) {
	ssinfo, err := ssModel.GetUserSSInfo(userID)
	if err != nil || ssinfo == nil {
		return
	}
	nodes, err := ssModel.FindNodes()
	if err != nil {
		return
	}
	ssm.StartTunnels(ssinfo, nodes)
}
