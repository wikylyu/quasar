package order

type CreateOrderFromProductRequest struct {
	ProductIds []string `json:"product_ids" form:"product_ids" query:"product_ids" validate:"required"`
}
