package product

import (
	log "github.com/sirupsen/logrus"
	"gitlab.gnome.org/wikylyu/quasar/errors"
	"gitlab.gnome.org/wikylyu/quasar/model/db"
)

func FindProducts() ([]*Product, error) {
	conn := db.PG()

	products := make([]*Product, 0)

	if err := conn.Model(&products).Where("status = ?", StatusOK).Order("sort").Select(); err != nil {
		log.Error("SQL Error:", err)
		return nil, errors.ErrDB
	}

	return products, nil
}
