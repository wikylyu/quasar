/*
 * Copyright (C) 2018 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package config

type GravityConfig struct {
	Addr     string     `json:"addr" yaml:"addr"`
	Username string     `json:"username" yaml:"username"`
	Password string     `json:"password" yaml:"password"`
	TLS      *TLSConfig `json:"tls" yaml:"tls"`
}

type TLSConfig struct {
	Enabled bool   `json:"enabled" yaml:"enabled"`
	Pem     string `json:"pem" yaml:"pem"`
	Key     string `json:"key" yaml:"key"`
}

type TunnelConfig struct {
	Name      string                 `json:"name" yaml:"name"`
	Addr      string                 `json:"addr" yaml:"addr"`
	Port      uint16                 `json:"port" yaml:"port"`
	Input     map[string]interface{} `json:"input" yaml:"input"`
	Output    map[string]interface{} `json:"output" yaml:"output"`
	AutoStart bool                   `json:"autoStart" yaml:"autoStart"`
	MaxConn   int32                  `json:"maxConn" yaml:"maxConn"` /* 最大的并发链接 */
}
