INSERT INTO "client"(id, name, token_max_age)
  VALUES('38beb27d-098b-41e4-8306-8175b05f9992', 'Web', 3600 * 24 * 15),
        ('9a7c83c1-33bd-4145-89ce-9e90c2a4b2bc', 'Android', 3600 * 24 * 365),
        ('decf7bf3-5305-44a7-a39a-442521e7e7ba','IOS', 3600 * 24 * 365);

-- 创建订单
INSERT INTO "product"(name, price, number_of_months, flow_limit_per_month)
  VALUES('基础套餐', 800, 1, 10*1024*1024*1024::bigint),
        ('高级套餐', 2200, 3, 100*1024*1024*1024::bigint);

-- 创建节点
INSERT INTO "node"(name, description, public_host, gravity_addr, gravity_port, gravity_username, gravity_password)
  VALUES('测试节点（湾湾）', '强烈推荐此节点，因为只有这个节点可用', '192.168.10.101', '127.0.0.1', 12121, 'abc', '123');
