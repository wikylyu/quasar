package ss

import (
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.gnome.org/wikylyu/quasar/errors"
	"gitlab.gnome.org/wikylyu/quasar/model/db"
)

func GetUserNodeMonthlyFlowStat(userID, nodeID string, year, month int) (*UserNodeMonthlyFlowStat, error) {
	conn := db.PG()

	t := time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.Local)

	var stat UserNodeMonthlyFlowStat
	if err := conn.Model(&stat).Where("user_id=? AND node_id=? AND mtime=?", userID, nodeID, t).Select(); err != nil {
		if err != db.ErrNoRows {
			log.Error("SQL Error:", err)
			return nil, errors.ErrDB
		}
		return nil, nil
	}
	stat.Year = stat.MTime.Year()
	stat.Month = int(stat.MTime.Month())
	return &stat, nil
}

func GetUserNodeHourlyFlowStat(userID, nodeID string, year, month, day, hour int) (*UserNodeHourlyFlowStat, error) {
	conn := db.PG()

	t := time.Date(year, time.Month(month), day, hour, 0, 0, 0, time.Local)

	var stat UserNodeHourlyFlowStat
	if err := conn.Model(&stat).Where("user_id=? AND node_id=? AND mtime=?", userID, nodeID, t).Select(); err != nil {
		if err != db.ErrNoRows {
			log.Error("SQL Error:", err)
			return nil, errors.ErrDB
		}
		return nil, nil
	}
	stat.Year = stat.MTime.Year()
	stat.Month = int(stat.MTime.Month())
	stat.Day = stat.MTime.Day()
	stat.Hour = stat.MTime.Hour()
	return &stat, nil
}

func AddUserNodeMonthlyFlowStat(userID, nodeID string, year, month int, ingress, egress uint64) error {
	conn := db.PG()

	t := time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.Local)

	stat := UserNodeMonthlyFlowStat{
		UserID:  userID,
		NodeID:  nodeID,
		MTime:   t,
		Ingress: ingress,
		Egress:  egress,
	}
	if _, err := conn.Model(&stat).OnConflict("(user_id, node_id, mtime) DO UPDATE").Set("ingress=user_node_monthly_flow_stat.ingress+?ingress").Set("egress=user_node_monthly_flow_stat.egress+?egress").Set("utime=CURRENT_TIMESTAMP").Insert(); err != nil {
		log.Error("SQL Error:", err)
		return errors.ErrDB
	}
	return nil
}

func AddUserMonthlyFlowStat(userID string, year, month int, ingress, egress uint64) error {
	conn := db.PG()

	t := time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.Local)

	stat := UserMonthlyFlowStat{
		UserID:  userID,
		MTime:   t,
		Ingress: ingress,
		Egress:  egress,
	}
	if _, err := conn.Model(&stat).OnConflict("(user_id, mtime) DO UPDATE").Set("ingress=user_monthly_flow_stat.ingress+?ingress").Set("egress=user_monthly_flow_stat.egress+?egress").Set("utime=CURRENT_TIMESTAMP").Insert(); err != nil {
		log.Error("SQL Error:", err)
		return errors.ErrDB
	}
	return nil
}

func AddUserNodeDailyFlowStat(userID, nodeID string, year, month, day int, ingress, egress uint64) error {
	conn := db.PG()

	t := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local)
	stat := UserNodeDailyFlowStat{
		UserID:  userID,
		NodeID:  nodeID,
		MTime:   t,
		Ingress: ingress,
		Egress:  egress,
	}
	if _, err := conn.Model(&stat).OnConflict("(user_id, node_id, mtime) DO UPDATE").Set("ingress=user_node_daily_flow_stat.ingress+?ingress").Set("egress=user_node_daily_flow_stat.egress+?egress").Set("utime=CURRENT_TIMESTAMP").Insert(); err != nil {
		log.Error("SQL Error:", err)
		return errors.ErrDB
	}
	return nil
}

func AddUserDailyFlowStat(userID string, year, month, day int, ingress, egress uint64) error {
	conn := db.PG()

	t := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local)
	stat := UserDailyFlowStat{
		UserID:  userID,
		MTime:   t,
		Ingress: ingress,
		Egress:  egress,
	}
	if _, err := conn.Model(&stat).OnConflict("(user_id, mtime) DO UPDATE").Set("ingress=user_daily_flow_stat.ingress+?ingress").Set("egress=user_daily_flow_stat.egress+?egress").Set("utime=CURRENT_TIMESTAMP").Insert(); err != nil {
		log.Error("SQL Error:", err)
		return errors.ErrDB
	}
	return nil
}

func AddUserNodeHourlyFlowStat(userID, nodeID string, year, month, day, hour int, ingress, egress uint64) error {
	conn := db.PG()

	t := time.Date(year, time.Month(month), day, hour, 0, 0, 0, time.Local)
	stat := UserNodeHourlyFlowStat{
		UserID:  userID,
		NodeID:  nodeID,
		MTime:   t,
		Ingress: ingress,
		Egress:  egress,
	}
	if _, err := conn.Model(&stat).OnConflict("(user_id, node_id, mtime) DO UPDATE").Set("ingress=user_node_hourly_flow_stat.ingress+?ingress").Set("egress=user_node_hourly_flow_stat.egress+?egress").Set("utime=CURRENT_TIMESTAMP").Insert(); err != nil {
		log.Error("SQL Error:", err)
		return errors.ErrDB
	}
	return nil
}

func AddUserHourlyFlowStat(userID string, year, month, day, hour int, ingress, egress uint64) error {
	conn := db.PG()

	t := time.Date(year, time.Month(month), day, hour, 0, 0, 0, time.Local)
	stat := UserHourlyFlowStat{
		UserID:  userID,
		MTime:   t,
		Ingress: ingress,
		Egress:  egress,
	}
	if _, err := conn.Model(&stat).OnConflict("(user_id, mtime) DO UPDATE").Set("ingress=user_hourly_flow_stat.ingress+?ingress").Set("egress=user_hourly_flow_stat.egress+?egress").Set("utime=CURRENT_TIMESTAMP").Insert(); err != nil {
		log.Error("SQL Error:", err)
		return errors.ErrDB
	}
	return nil
}

func FindUserNodeHourlyFlowStat(userID, nodeID string, t time.Time) ([]*UserNodeHourlyFlowStat, error) {
	conn := db.PG()

	stats := make([]*UserNodeHourlyFlowStat, 0)
	if err := conn.Model(&stats).Where("user_id=? AND node_id=? AND mtime>=?", userID, nodeID, t).Select(); err != nil {
		log.Error("SQL Error:", err)
		return nil, errors.ErrDB
	}
	for _, st := range stats {
		st.Year = st.MTime.Year()
		st.Month = int(st.MTime.Month())
		st.Day = st.MTime.Day()
		st.Hour = st.MTime.Hour()
	}
	return stats, nil
}

func FindUserHourlyFlowStat(userID string, t time.Time) ([]*UserHourlyFlowStat, error) {
	conn := db.PG()

	stats := make([]*UserHourlyFlowStat, 0)
	if err := conn.Model(&stats).Where("user_id=? AND mtime>=?", userID, t).Select(); err != nil {
		log.Error("SQL Error:", err)
		return nil, errors.ErrDB
	}
	for _, st := range stats {
		st.Year = st.MTime.Year()
		st.Month = int(st.MTime.Month())
		st.Day = st.MTime.Day()
		st.Hour = st.MTime.Hour()
	}
	return stats, nil
}

func FindUserDailyFlowStats(userID string, t time.Time) ([]*UserDailyFlowStat, error) {
	conn := db.PG()

	stats := make([]*UserDailyFlowStat, 0)
	if err := conn.Model(&stats).Where("user_id=? AND mtime>=?", userID, t).Select(); err != nil {
		log.Error("SQL Error:", err)
		return nil, errors.ErrDB
	}
	for _, st := range stats {
		st.Year = st.MTime.Year()
		st.Month = int(st.MTime.Month())
		st.Day = st.MTime.Day()
	}
	return stats, nil
}

func FindUserMonthlyFlowStats(userID string, t time.Time) ([]*UserMonthlyFlowStat, error) {
	conn := db.PG()

	stats := make([]*UserMonthlyFlowStat, 0)
	if err := conn.Model(&stats).Where("user_id=? AND mtime>=?", userID, t).Select(); err != nil {
		log.Error("SQL Error:", err)
		return nil, errors.ErrDB
	}
	for _, st := range stats {
		st.Year = st.MTime.Year()
		st.Month = int(st.MTime.Month())
	}
	return stats, nil
}
