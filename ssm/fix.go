package ssm

import (
	log "github.com/sirupsen/logrus"
	"gitlab.gnome.org/wikylyu/galaxy/tunnel"
	ssModel "gitlab.gnome.org/wikylyu/quasar/model/ss"
)

/* 检查用户服务是否都正常，如果不正常则修复 */
func FixTunnels() {
	ssinfos, err := ssModel.FindUserSSInfos()
	if err != nil {
		log.Error("FindUserSSInfos failed:", err)
		return
	}
	nodes, err := ssModel.FindNodes()
	if err != nil {
		log.Error("FindNodes failed:", err)
		return
	}
	for _, n := range nodes {
		for _, ssinfo := range ssinfos {
			status, err := addTunnel(ssinfo, n)
			if err != nil {
				log.Error("addTunnel failed:", err)
			} else if status == tunnel.StatusOK || status == tunnel.StatusTunnelNameExists {
				log.Infof("Tunnel %s added", ssinfo.UserID)
			} else {
				log.Infof("Tunnel %s unknown error %v", status)
			}
			service, err := ssModel.GetCurrentUserService(ssinfo.UserID)
			if err != nil {
				log.Error("GetCurrentUserService failed:", err)
			} else if service != nil && service.Status <= ssModel.ServiceStatusRunning {
				status, err := startTunnel(ssinfo, n)
				if err != nil {
					log.Error("startTunnel failed:", err)
				} else if status == tunnel.StatusOK {
					log.Infof("Tunnel %s started", ssinfo.UserID)
				} else {
					log.Infof("Tunnel %s: unknown error %v", ssinfo.UserID, status)
				}
			}
		}
	}
}
