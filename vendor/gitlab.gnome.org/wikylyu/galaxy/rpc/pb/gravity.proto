/*
 * Copyright (C) 2018 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */
syntax = "proto3";
package pb;

// The greeting service definition.
service Gravity {
  rpc Version(Void) returns (VersionReply) {}
  rpc AddTunnel(AddTunnelRequest) returns (CommonReply) {}
  rpc RemoveTunnel(RemoveTunnelRequest) returns (CommonReply) {}
  rpc StartTunnel(StartTunnelRequest) returns (CommonReply) {}
  rpc StopTunnel(StopTunnelRequest) returns (CommonReply) {}
  rpc ListTunnel(ListTunnelRequest) returns (ListTunnelReply) {}
  rpc StatTunnel(StatTunnelRequest) returns (StatTunnelReply) {}
  rpc UpdateTunnel(UpdateTunnelRequest) returns (CommonReply) {}
}

message Void {
}

message VersionReply {
	string appName = 1;
	string appVersion = 2;
	string appBuild = 3;
	string appCommit = 4;
}

message CommonReply {
	uint32 status = 1;
}

message AddTunnelRequest {
	string name = 1;
	string addr = 2;
	uint32 port = 3;
	string input = 4;
	string output = 5;
	int32 maxConn = 6;
}

message UpdateTunnelRequest {
	string name = 1;
	string addr = 2;
	uint32 port = 3;
	string input = 4;
	string output = 5;
	int32 maxConn = 6;
}

message StartTunnelRequest {
	string name = 1;
	bool ignoreRunning = 2;	// 忽略已运行错误
}

message StopTunnelRequest {
	string name = 1;
	bool ignoreIdle = 2;	// 忽略未运行错误
}

message RemoveTunnelRequest {
	string name = 1;
	bool force = 2;	// 如果正在运行，则强制关闭
}

message ListTunnelRequest {
	string query = 1;
	bool options = 2;
}

message ListTunnelReply {
	repeated TunnelInfo tunnels = 2;
}

enum TunnelStatus {
	Idle = 0;
	Running = 1;
	Error = 2;
}

message TunnelInfo {
	string name = 1;
	string addr = 2;
	uint32 port = 3;
	TunnelStatus status = 4;
	int32 MaxConn = 5;
	int32 CurConn = 6;
	string iProtocol = 7;
	map<string, string> iOptions = 8;
	string oProtocol = 9;
	map<string, string> oOptions = 10;

	uint64 startTime = 11;
}

message FlowStat {
	uint32 year = 1;
	uint32 month = 2;
	uint32 day = 3;
	uint32 hour = 4;
	uint64 ingress = 5;
	uint64 egress = 6;
}


message StatTunnelRequest {
	string name = 1;
	string from = 2;
	string to = 3;
}

message StatTunnelReply {
	uint32 status = 1;
	repeated FlowStat flowStats = 2;
}
