package controller

import (
	"gitlab.gnome.org/wikylyu/quasar/controller/account"
	"gitlab.gnome.org/wikylyu/quasar/controller/file"
	"gitlab.gnome.org/wikylyu/quasar/controller/middleware"
	"gitlab.gnome.org/wikylyu/quasar/controller/order"
	"gitlab.gnome.org/wikylyu/quasar/controller/product"
	"gitlab.gnome.org/wikylyu/quasar/controller/ss"

	"github.com/go-playground/validator"
	"github.com/labstack/echo"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func Register(e *echo.Echo) {
	e.Validator = &CustomValidator{validator: validator.New()}

	api := e.Group("/api", middleware.SourceMiddleware)
	api.POST("/account", account.Signup)     // 用户注册
	api.PUT("/account/token", account.Login) // 用户登录

	authAPI := api.Group("", middleware.AuthMiddleware)
	authAPI.GET("/account", account.GetAccountInfo)
	authAPI.DELETE("/account/token", account.Logout) // 用户登出

	/* 订单相关 */
	api.GET("/products", product.FindProducts)
	authAPI.POST("/order", order.CreateOrderFromProducts)
	authAPI.GET("/order/:id", order.GetOrder)
	authAPI.GET("/pay/order/:id", order.GetOrderPayInfo)

	/* SS相关 */
	api.GET("/ss/nodes", ss.FindNodes)
	authAPI.GET("/ss/info", ss.GetUserSSInfo)
	authAPI.GET("/ss/service/current", ss.GetCurrentUserService)
	authAPI.GET("/ss/flow/stats", ss.FindUserFlowStats)
	// 上传文件
	authAPI.POST("/file", file.UploadFile)
}
