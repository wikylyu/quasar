FROM scratch AS star

ADD ./bin/star /star
ADD ./quasar.yaml /quasar.yaml

WORKDIR /
ENTRYPOINT [ "/star" ]


FROM scratch AS quasar

ADD ./bin/quasar /quasar
ADD ./quasar.yaml /quasar.yaml

EXPOSE 2313

WORKDIR /
ENTRYPOINT [ "/quasar" ]
