package client

const (
	KeyClient = "quasar.client"
)

func getClientKey(clientID string) string {
	return KeyClient + "." + clientID
}
