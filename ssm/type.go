package ssm

type FlowStat struct {
	Year    uint32
	Month   uint32
	Day     uint32
	Hour    uint32
	Ingress uint64
	Egress  uint64
}
