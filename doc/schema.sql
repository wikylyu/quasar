-- DBTYPE: POSTGRESQL

CREATE DATABASE quasar WITH ENCODING='UTF8';
\c quasar;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";


-- 客户端配置
CREATE TABLE "client" (
  id UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v1mc(), -- 客户端ID
  name VARCHAR(32) NOT NULL,						-- 客户端名称
  token_max_age INTEGER NOT NULL,					-- 登录有效时长，单位秒
  ctime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

-- 账号
CREATE TABLE "account"(
  id UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
  email VARCHAR(32) NOT NULL,
  salt VARCHAR(32) NOT NULL,
  passwd VARCHAR(256) NOT NULL,
  ptype VARCHAR(32) NOT NULL, -- 密码的加密方式，目前支持 'MD5','SHA1', 'SHA256'
  utype VARCHAR(32) NOT NULL, -- 用户类型，目前支持 'customer', 'admin'
  status INTEGER NOT NULL DEFAULT 0, -- 账号状态 0表示正常
  utime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  ctime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  UNIQUE(email)
);

-- Token
CREATE TABLE "token"(
  id UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
  user_id UUID NOT NULL,
  client_id UUID NOT NULL,
  etime TIMESTAMP WITH TIME ZONE,
  ctime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  UNIQUE("user_id", "client_id")
);
CREATE INDEX token__user_id__client_id ON "token"(user_id, client_id);

-- 用户信息
CREATE TABLE "user_profile"(
  user_id UUID PRIMARY KEY,
  name VARCHAR(32) NOT NULL DEFAULT '', -- '昵称'
  avatar VARCHAR(64) NOT NULL DEFAULT '', -- 头像的图片ID
  cover VARCHAR(64) NOT NULL DEFAULT '',
  utime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  ctime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

-- 节点信息
CREATE TABLE "node"(
  id UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
  name VARCHAR(32) NOT NULL, -- 节点名
  description VARCHAR(256) NOT NULL, -- 节点描述
  public_host VARCHAR(128) NOT NULL,   -- 公网地址，IP或者域名，
  gravity_addr VARCHAR(128) NOT NULL, -- 内网管理IP
  gravity_port INTEGER NOT NULL, -- 内网管理端口
  gravity_username VARCHAR(32) NOT NULL, -- 管理用户名
  gravity_password VARCHAR(64) NOT NULL, -- 管理密码,
  utime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  ctime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "product"(
  id UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
  name VARCHAR(32) NOT NULL, -- 商品名
  price BIGINT NOT NULL, -- 价格，单位分
  number_of_months INTEGER NOT NULL, -- 服务时长，单位月
  flow_limit_per_month BIGINT NOT NULL, -- 每个月可以使用的流量
  status INTEGER NOT NULL DEFAULT 0, -- 商品状态，0表示正常，1表示禁用
  sort INTEGER NOT NULL DEFAULT 0, -- 商品排序
  utime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  ctime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX product__status ON product(status);
CREATE INDEX product__sort ON product(sort);

CREATE TABLE "order"(
  id UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
  price BIGINT NOT NULL, -- 总价
  user_id UUID NOT NULL,
  status INTEGER NOT NULL DEFAULT 0, -- 订单状态，0表示未支付，1表示已支付
  utime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  ctime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "order_item"(
  id UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
  order_id UUID NOT NULL,
  product_id UUID NOT NULL,
  user_id UUID NOT NULL,
  utime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  ctime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "order_item_snapshot"(
  order_item_id UUID NOT NULL,
  name VARCHAR(32) NOT NULL, -- 商品名
  price BIGINT NOT NULL, -- 价格，单位分
  number_of_months INTEGER NOT NULL, -- 服务时长，单位月
  flow_limit_per_month BIGINT NOT NULL, -- 每个月可以使用的流量
  utime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  ctime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);


-- [startime, endtime)
CREATE TABLE "user_service"(
  id UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v1mc(),
  user_id UUID NOT NULL,
  starttime TIMESTAMP WITH TIME ZONE,
  endtime TIMESTAMP WITH TIME ZONE,
  flow_limit BIGINT NOT NULL, -- 流量限制
  flow_used BIGINT NOT NULL DEFAULT 0, -- 已使用的流量
  status INTEGER NOT NULL DEFAULT 0, -- 状态，0未开始，1进行中，2已结束，3流量限制
  utime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  ctime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX user_service__user_id ON user_service(user_id);
CREATE INDEX user_serivce__starttime_endtime ON user_service(starttime, endtime);

-- 用户的SS信息
CREATE TABLE "user_ss_info"(
  user_id UUID NOT NULL PRIMARY KEY, -- 用户ID
  port SERIAL NOT NULL , -- SS端口
  passwd VARCHAR(128) NOT NULL, -- SS密码
  method VARCHAR(128) NOT NULL, -- SS加密方式
  utime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  ctime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  UNIQUE(port)
);
ALTER SEQUENCE user_ss_info_port_seq RESTART WITH 10000;

-- 用户的流量统计
CREATE TABLE "user_node_monthly_flow_stat"(
  id UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
  user_id UUID NOT NULL,
  node_id UUID NOT NULL,
  mtime TIMESTAMP WITH TIME ZONE, -- 时间，只精确到月份，该时间表示每月一号零点零分
  ingress BIGINT NOT NULL, -- 上行流量，单位B
  egress BIGINT NOT NULL, -- 下行流量，单位B 
  utime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  ctime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  UNIQUE(user_id, node_id, mtime)
);

CREATE TABLE "user_monthly_flow_stat"(
  id UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
  user_id UUID NOT NULL,
  mtime TIMESTAMP WITH TIME ZONE, -- 时间，只精确到月份，该时间表示每月一号零点零分
  ingress BIGINT NOT NULL, -- 上行流量，单位B
  egress BIGINT NOT NULL, -- 下行流量，单位B 
  utime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  ctime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  UNIQUE(user_id, mtime)
);

CREATE TABLE "user_node_daily_flow_stat"(
  id UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
  user_id UUID NOT NULL,
  node_id UUID NOT NULL,
  mtime TIMESTAMP WITH TIME ZONE, -- 时间，只精确到日，该时间表示每日零点零分
  ingress BIGINT NOT NULL, -- 上行流量，单位B
  egress BIGINT NOT NULL, -- 下行流量，单位B 
  utime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  ctime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  UNIQUE(user_id, node_id, mtime)
);

CREATE TABLE "user_daily_flow_stat"(
  id UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
  user_id UUID NOT NULL,
  mtime TIMESTAMP WITH TIME ZONE, -- 时间，只精确到日，该时间表示每日零点零分
  ingress BIGINT NOT NULL, -- 上行流量，单位B
  egress BIGINT NOT NULL, -- 下行流量，单位B 
  utime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  ctime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  UNIQUE(user_id, mtime)
);

CREATE TABLE "user_node_hourly_flow_stat"(
  id UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
  user_id UUID NOT NULL,
  node_id UUID NOT NULL,
  mtime TIMESTAMP WITH TIME ZONE, -- 时间，只精确到小时，该时间表示每小时零分
  ingress BIGINT NOT NULL, -- 上行流量，单位B
  egress BIGINT NOT NULL, -- 下行流量，单位B 
  utime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  ctime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  UNIQUE(user_id, node_id, mtime)
);

CREATE TABLE "user_hourly_flow_stat"(
  id UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
  user_id UUID NOT NULL,
  mtime TIMESTAMP WITH TIME ZONE, -- 时间，只精确到小时，该时间表示每小时零分
  ingress BIGINT NOT NULL, -- 上行流量，单位B
  egress BIGINT NOT NULL, -- 下行流量，单位B 
  utime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  ctime TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  UNIQUE(user_id, mtime)
);
