package ssm

import (
	"context"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.gnome.org/wikylyu/galaxy/rpc/pb"
	"gitlab.gnome.org/wikylyu/galaxy/tunnel"
	ssModel "gitlab.gnome.org/wikylyu/quasar/model/ss"
)

func addTunnel(info *ssModel.UserSSInfo, n *ssModel.Node) (uint32, error) {
	c := getGravityClient(n)
	if c == nil {
		return 0, fmt.Errorf("cannot connect to %s", n.String())
	}
	input := fmt.Sprintf(`{"protocol":"shadowsocks","method":"%s","password":"%s"}`, info.Method, info.Password)
	output := `{"protocol":"direct"}`
	req := &pb.AddTunnelRequest{
		Name:   info.UserID,
		Addr:   "",
		Port:   uint32(info.Port),
		Input:  input,
		Output: output,
	}
	reply, err := c.AddTunnel(context.Background(), req)
	if err != nil {
		return 0, err
	}
	return reply.Status, nil
}

func AddTunnel(info *ssModel.UserSSInfo, n *ssModel.Node) error {
	status, err := addTunnel(info, n)
	if err != nil {
		log.Errorf("Add Tunnel %s@%s Error:%v", info.UserID, n.String(), err)
	} else if status != tunnel.StatusOK {
		log.Errorf("Add Tunnel %s@%s failed:", info.UserID, n.String(), status)
	} else {
		log.Infof("Add Tunnel %s@%s successfully", info.UserID, n.String())
	}
	return nil
}

func AddTunnels(info *ssModel.UserSSInfo, nodes []*ssModel.Node) error {
	for _, n := range nodes {
		AddTunnel(info, n)
	}
	return nil
}

func startTunnel(info *ssModel.UserSSInfo, n *ssModel.Node) (uint32, error) {
	c := getGravityClient(n)
	if c == nil {
		return 0, fmt.Errorf("cannot connect to %s", n.String())
	}
	req := &pb.StartTunnelRequest{
		Name:          info.UserID,
		IgnoreRunning: true,
	}
	reply, err := c.StartTunnel(context.Background(), req)
	if err != nil {
		return 0, err
	}
	return reply.Status, nil
}

func StartTunnel(info *ssModel.UserSSInfo, n *ssModel.Node) error {
	status, err := startTunnel(info, n)
	if err != nil {
		log.Errorf("Start Tunnel %s@%s Error:%v", info.UserID, n.String(), err)
	} else if status != tunnel.StatusOK {
		log.Errorf("Start Tunnel %s@%s failed:%d", info.UserID, n.String(), status)
	} else {
		log.Infof("Start Tunnel %s@%s successfully", info.UserID, n.String())
	}
	return nil
}

func StartTunnels(info *ssModel.UserSSInfo, nodes []*ssModel.Node) error {
	for _, n := range nodes {
		StartTunnel(info, n)
	}
	return nil
}

func StopTunnel(info *ssModel.UserSSInfo, n *ssModel.Node) error {
	c := getGravityClient(n)
	if c == nil {
		log.Error("Cannot connect to %s", n.String())
		return nil
	}
	name := info.UserID
	req := &pb.StopTunnelRequest{
		Name:       name,
		IgnoreIdle: true,
	}
	reply, err := c.StopTunnel(context.Background(), req)
	if err != nil {
		log.Errorf("Stop Tunnel %s@%s Error:%v", name, n.String(), err)
	} else if reply.Status != tunnel.StatusOK {
		log.Errorf("Stop Tunnel %s@%s failed:%d", name, n.String(), reply.Status)
	} else {
		log.Infof("Stop Tunnel %s@%s successfully", name, n.String())
	}
	return nil
}

func StopTunnels(info *ssModel.UserSSInfo, nodes []*ssModel.Node) error {
	for _, n := range nodes {
		StopTunnel(info, n)
	}
	return nil
}

func StatTunnel(info *ssModel.UserSSInfo, n *ssModel.Node, year, month, day, hour int) (*FlowStat, error) {
	c := getGravityClient(n)
	if c == nil {
		log.Error("Cannot connect to %s", n.String())
		return nil, nil
	}
	name := info.UserID
	from := time.Date(year, time.Month(month), day, hour, 0, 0, 0, time.Local)
	req := &pb.StatTunnelRequest{
		Name: name,
		From: from.Format(time.RFC3339),
		To:   from.Format(time.RFC3339),
	}
	reply, err := c.StatTunnel(context.Background(), req)
	if err != nil {
		log.Errorf("Stat Tunnel %s@%s Error:%v", name, n.String(), err)
		return nil, err
	} else if reply.Status != tunnel.StatusOK {
		log.Errorf("Stat Tunnel %s@%s failed:%d", name, n.String(), reply.Status)
		return nil, nil
	} else if len(reply.FlowStats) == 0 {
		return nil, nil
	}
	st := reply.FlowStats[0]
	return &FlowStat{
		Year:    st.Year,
		Month:   st.Month,
		Day:     st.Day,
		Hour:    st.Hour,
		Ingress: st.Ingress,
		Egress:  st.Egress,
	}, nil
}
