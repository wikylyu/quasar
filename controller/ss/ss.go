package ss

import (
	"net/http"
	"time"

	"github.com/labstack/echo"
	"gitlab.gnome.org/wikylyu/quasar/controller/context"
	"gitlab.gnome.org/wikylyu/quasar/errors"
	ssModel "gitlab.gnome.org/wikylyu/quasar/model/ss"
)

func FindNodes(c echo.Context) error {
	ctx := c.(*context.Context)

	nodes, err := ssModel.FindNodes()
	if err != nil {
		return err
	}
	return ctx.JSON(http.StatusOK, nodes)
}

func GetUserSSInfo(c echo.Context) error {
	ctx := c.(*context.Context)

	ssinfo, err := ssModel.GetUserSSInfo(ctx.Account.ID)
	if err != nil {
		return err
	} else if ssinfo == nil {
		return errors.ErrNoSSInfo
	}
	return ctx.JSON(http.StatusOK, ssinfo)
}

func GetCurrentUserService(c echo.Context) error {
	ctx := c.(*context.Context)

	us, err := ssModel.GetCurrentUserService(ctx.Account.ID)
	if err != nil {
		return err
	} else if us == nil {
		return errors.ErrNoService
	}
	return ctx.JSON(http.StatusOK, us)
}

/* 获取流量统计，根据参数period获取不同周期的流量数据 */
func FindUserFlowStats(c echo.Context) error {
	ctx := c.(*context.Context)

	var data FindUserFlowStatsRequest
	if err := ctx.Bind(&data); err != nil {
		return err
	}
	var stats interface{}
	var err error = errors.ErrParamsInvalid
	
	if data.Period == FlowPeriodHourly {
		t := time.Now().Add(time.Hour * time.Duration(-data.Hours))
		stats, err = ssModel.FindUserHourlyFlowStat(ctx.Account.ID, t)
	} else if data.Period == FlowPeriodDaily {
		t := time.Now().AddDate(0, 0, -data.Days)
		stats, err = ssModel.FindUserDailyFlowStats(ctx.Account.ID, t)
	} else if data.Period == FlowPeriodMonthly {
		t := time.Now().AddDate(0, -data.Months, 0)
		stats, err = ssModel.FindUserMonthlyFlowStats(ctx.Account.ID, t)
	}
	if err != nil {
		return err
	}
	return ctx.JSON(http.StatusOK, stats)
}
