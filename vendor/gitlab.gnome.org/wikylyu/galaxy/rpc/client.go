/*
 * Copyright (C) 2018 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package rpc

import (
	"context"
	"crypto/tls"

	"gitlab.gnome.org/wikylyu/galaxy/rpc/pb"

	"google.golang.org/grpc"
	"google.golang.org/grpc/connectivity"
	"google.golang.org/grpc/credentials"
)

type GravityClient struct {
	pb.GravityClient
	conn *grpc.ClientConn
}

func (c *GravityClient) GetState() connectivity.State {
	return c.conn.GetState()
}

func grpcDial(host string, secure bool, username, password string) (*grpc.ClientConn, error) {
	opts := make([]grpc.DialOption, 0)
	if secure {
		creds := credentials.NewTLS(&tls.Config{
			InsecureSkipVerify: true,
		})
		opts = append(opts, grpc.WithTransportCredentials(creds))
	} else {
		opts = append(opts, grpc.WithInsecure())
	}
	opts = append(opts, grpc.WithPerRPCCredentials(&authCreds{Username: username, Password: password}))
	return grpc.Dial(host, opts...)
}

func NewGravityClient(host string, secure bool, username, password string) (*GravityClient, error) {
	conn, err := grpcDial(host, secure, username, password)
	if err != nil {
		return nil, err
	}
	c := pb.NewGravityClient(conn)

	c.Version(context.Background(), &pb.Void{})
	return &GravityClient{
		GravityClient: c,
		conn:          conn,
	}, nil
}

func (c *GravityClient) Close() {
	defer c.conn.Close()
}

type authCreds struct {
	Username, Password string
}

func (c *authCreds) GetRequestMetadata(context.Context, ...string) (map[string]string, error) {
	return map[string]string{
		"username": c.Username,
		"password": c.Password,
	}, nil
}

func (c *authCreds) RequireTransportSecurity() bool {
	return false
}
