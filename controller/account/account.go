package account

import (
	"net/http"
	"time"

	accountCache "gitlab.gnome.org/wikylyu/quasar/cache/account"
	"gitlab.gnome.org/wikylyu/quasar/controller/context"
	"gitlab.gnome.org/wikylyu/quasar/errors"
	accountModel "gitlab.gnome.org/wikylyu/quasar/model/account"
	"gitlab.gnome.org/wikylyu/quasar/util"

	"github.com/gorilla/sessions"
	"github.com/labstack/echo"
	"github.com/labstack/echo-contrib/session"
)

func login(ctx *context.Context, a *accountModel.Account) error {
	client := ctx.Client
	maxAge := client.TokenMaxAge // 单位秒

	/* 更新Token */
	tk, err := accountModel.UpsertToken(a.ID, ctx.Client.ID, time.Second*time.Duration(maxAge))
	if err != nil {
		return err
	}

	sess, _ := session.Get("session", ctx)
	sess.Options = &sessions.Options{
		Path:   "/",
		MaxAge: int(maxAge),
	}
	sess.Values["token"] = tk.ID
	sess.Save(ctx.Request(), ctx.Response())
	return ctx.JSON(http.StatusOK, tk)
}

/* 登录 */
func Login(c echo.Context) error {
	ctx := c.(*context.Context)
	data := LoginRequest{}
	if err := ctx.Bind(&data); err != nil {
		return err
	} else if err := ctx.Validate(&data); err != nil {
		return err
	}

	email := data.Email
	password := data.Password

	a, err := accountModel.GetAccountByEmail(email)
	if err != nil {
		return err
	} else if a == nil { /* 用户不存在 */
		return errors.ErrUserNotFound
	}
	if !a.Auth(password) { /* 密码错误 */
		return errors.ErrPasswordIncorrect
	}

	return login(ctx, a)
}

func Logout(c echo.Context) error {
	ctx := c.(*context.Context)

	if err := accountCache.DeleteToken(ctx.Token.ID); err != nil {
		return err
	}

	sess, _ := session.Get("session", ctx)
	sess.Values["token"] = ""
	sess.Save(ctx.Request(), ctx.Response())
	return ctx.NoContent(http.StatusOK)
}

/* 注册 */
func Signup(c echo.Context) error {
	ctx := c.(*context.Context)
	data := SignupRequest{}
	if err := ctx.Bind(&data); err != nil {
		return err
	} else if err := ctx.Validate(&data); err != nil {
		return err
	}
	email := data.Email
	password := data.Password

	if len(email) <= 5 || len(email) >= 32 || !util.ValidateEmail(email) {
		return errors.ErrEmailInvalid
	}

	if len(password) <= 4 || len(password) >= 12 {
		return errors.ErrPasswordInvalid
	}

	a, err := accountModel.CreateAccount(email, password, accountModel.PTypeSHA256)
	if err != nil {
		return err
	}

	return login(ctx, a)
}

/* 获取帐号信息 */
func GetAccountInfo(c echo.Context) error {
	ctx := c.(*context.Context)
	return ctx.JSON(http.StatusOK, ctx.Account)
}
