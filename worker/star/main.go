package main

import (
	"os"
	"os/signal"

	log "github.com/sirupsen/logrus"
	"gitlab.gnome.org/wikylyu/quasar/config"
	"gitlab.gnome.org/wikylyu/quasar/logging"
	"gitlab.gnome.org/wikylyu/quasar/model/db"
)

const APPNAME = "quasar"

func main() {
	initConfig()
	initLogging()
	initDatabase()

	go run()

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt, os.Kill)
	<-quit
}

func initConfig() {
	config.Init(APPNAME)
}

func initLogging() {
	format := config.GetStringDefault("log.format", "json")
	level := config.GetStringDefault("log.level", "info")
	output := config.GetStringDefault("log.output", "stdout")

	logging.Init(format, level, output)
}

/* 初始化数据库 */
func initDatabase() {
	pgURL := config.GetString("database.pg.url")
	mongoURL := config.GetString("database.mongo.url")
	mongoDBName := config.GetString("database.mongo.dbname")
	if pgURL == "" {
		log.Fatal("**CONFIG** database.pg.url not found")
	} else if mongoURL == "" {
		log.Fatal("**CONFIG** database.mongo.url not found")
	} else if mongoDBName == "" {
		log.Fatal("**CONFIG** database.mongo.dbname not found")
	}
	if err := db.Init(pgURL, mongoURL, mongoDBName); err != nil {
		log.Fatal("Connecting database failed:", err)
	}
}
