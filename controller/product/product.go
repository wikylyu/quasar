package product

import (
	"net/http"

	"gitlab.gnome.org/wikylyu/quasar/controller/context"
	productModel "gitlab.gnome.org/wikylyu/quasar/model/product"

	"github.com/labstack/echo"
)

func FindProducts(c echo.Context) error {
	ctx := c.(*context.Context)
	products, err := productModel.FindProducts()
	if err != nil {
		return err
	}
	return ctx.JSON(http.StatusOK, products)
}
