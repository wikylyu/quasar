package order

import "time"

const (
	StatusUnpaid    = 0
	StatusPaid      = 1
	StatusCancelled = -1
)

type Order struct {
	TableName struct{}  `sql:"\"order\"" json:"-"`
	ID        string    `sql:"id,pk" json:"id"`
	Price     uint64    `sql:"price" json:"price"`
	UserID    string    `sql:"user_id" json:"user_id"`
	Status    int       `sql:"status" json:"status"`
	UTIme     time.Time `sql:"utime,null" json:"utime"`
	CTime     time.Time `sql:"ctime,null" json:"ctime"`
}

type OrderItem struct {
	TableName struct{}  `sql:"order_item" json:"-"`
	ID        string    `sql:"id,pk" json:"id"`
	OrderID   string    `sql:"order_id" json:"order_id"`
	ProductID string    `sql:"product_id" json:"product_id"`
	UserId    string    `sql:"user_id" json:"user_id"`
	UTIme     time.Time `sql:"utime,null" json:"utime"`
	CTime     time.Time `sql:"ctime,null" json:"ctime"`
}

type OrderItemSnapshot struct {
	TableName         struct{}  `sql:"order_item_snapshot" json:"-"`
	OrderItemID       string    `sql:"order_item_id,pk" json:"id"`
	Name              string    `sql:"name" json:"name"`
	Price             uint64    `sql:"price" json:"price"`
	NumberOfMonths    uint      `sql:"number_of_months" json:"number_of_months"`
	FlowLimitPerMonth uint64    `sql:"flow_limit_per_month" json:"flow_limit_per_month"`
	UTIme             time.Time `sql:"utime,null" json:"utime"`
	CTime             time.Time `sql:"ctime,null" json:"ctime"`
}
