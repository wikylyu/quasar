/*
 * Copyright (C) 2018 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package gnet

import (
	"io"
	"net"
)

type IConn interface {
	Read() ([]byte, error)
	Write([]byte) error
}

func ConnChannel(gc IConn) chan []byte {
	c := make(chan []byte, 2048)
	go func() {
		defer close(c)
		for {
			if data, err := gc.Read(); err != nil {
				break
			} else {
				c <- data
			}
		}
	}()
	return c
}

type GConn struct {
	net.Conn
}

func Dial(network, address string) (*GConn, error) {
	c, err := net.Dial(network, address)
	if err != nil {
		return nil, err
	}
	return &GConn{
		Conn: c,
	}, nil
}

func (c *GConn) Write(data []byte) error {
	total := len(data)
	written := 0
	for written < total {
		if n, err := c.Conn.Write(data[written:]); err != nil {
			return err
		} else {
			written += n
		}
	}
	return nil
}

func (c *GConn) Read() ([]byte, error) {
	buf := make([]byte, 4096)
	if n, err := c.Conn.Read(buf); err != nil {
		return nil, err
	} else {
		return buf[:n], nil
	}
}

func (c *GConn) ReadFull(n int) ([]byte, error) {
	buf := make([]byte, n)
	if _, err := io.ReadFull(c.Conn, buf); err != nil {
		return nil, err
	}
	return buf, nil
}
